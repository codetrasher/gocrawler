// gocrawler project main.go
package main

import (
	"flag"
	"fmt"
	//	"fmt"

	"bitbucket.org/codetrasher/crawler"
)

var (
	root    *string = flag.String("root", "", "Root page to start crawling.")
	pattern *string = flag.String("pattern", "href", `A set of patterns that crawler uses to parse data from
	a web page.Default pattern is "href". If you wish to add more detailed link scraping patterns, use following format:
	"href|subpath=nameofsubpath|keyword=keyword_in_link".`)
	depth     *int   = flag.Int("depth", 0, "Defines the depth where crawler parses data from.")
	userAgent string = "GoCrawler/0.1 (https://bitbucket.org/codetrasher/gocrawler)"
)

func main() {

	flag.Parse()

	if *root != "" {
		crawler.Build(*root, *depth, *pattern, userAgent)
		crawler.Run()
	} else {
		fmt.Println("Can't start crawler with empty root page value!")
	}
}
